from rest_framework import serializers
from .models import *

class MaterielSerializer(serializers.ModelSerializer):
    class Meta:
        model = Materiel
        fields = '__all__'
    
    def validate_nom(self, value):
        method = None
        request = self.context.get("request")
        if request and hasattr(request, "method"):
            method = request.method
        qs = Materiel.objects.filter(nom__iexact=value)
        if qs.exists() and request.method == 'POST':
            raise serializers.ValidationError("Nom deja pris")
        
        return value

class CategorieSerializer(serializers.ModelSerializer):
    #categorie = serializers.ReadOnlyField(source='categorie.type')
    class Meta:
        model = Categorie
        fields = '__all__'
    
    def validate_type(self, value):
        request = self.context.get("request")
        if request and hasattr(request, "method"):
            method = request.method
        qs = Categorie.objects.filter(type__iexact=value)
        if qs.exists() and request.method == 'POST':
            raise serializers.ValidationError("Nom deja pris")
        
        return value

class SalleSerializer(serializers.ModelSerializer):
    #client = serializers.ReadOnlyField(source='client.nom')
    #clients = ClientSerializer(many=True, read_only=True)
    #Materiel = serializers.ReadOnlyField(source='Materiel.nom')
    #categorie = serializers.ReadOnlyField(source='categorie.nom')
    class Meta:
        model = Salle
        #fields = ('numero', 'categorie', 'description', 'Materiel','client')
        fields = "__all__"

        
    
    def validate_numero(self, value):
        method = None
        request = self.context.get("request")
        if request and hasattr(request, "method"):
            method = request.method
        qs = Salle.objects.filter(numero=value)
        
        if qs.exists() and request.method == 'POST':
            raise serializers.ValidationError("Salle déjà ajoutée")
                
        return value

class ClientSerializer(serializers.ModelSerializer):
    #salles = SalleSerializer(many=True, read_only=True)
    #salles = serializers.ReadOnlyField(source='salle.numero')
    #salles = SalleSerializer(source='salle_set',many=True, read_only=True)
    class Meta:
        model = Client
        #depth = 1
        #fields = ('id', 'nom')
        fields = "__all__"
    
    def validate_nom(self, value):
        qs = Client.objects.filter(nom__iexact=value)
        if qs.exists():
            raise serializers.ValidationError("Nom deja pris")
        
        return value

class ReservationSerializer(serializers.ModelSerializer):
    #client = serializers.ReadOnlyField(source='client.nom')
    #Salle = serializers.ReadOnlyField(source='Salle.numero')
    class Meta:
        model = Reservation
        fields = "__all__"