# Generated by Django 3.0 on 2021-11-09 13:45

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0006_merge_20210108_1729'),
    ]

    operations = [
        migrations.RenameField(
            model_name='categorie',
            old_name='date_created',
            new_name='created_at',
        ),
        migrations.RenameField(
            model_name='materiel',
            old_name='date_created',
            new_name='created_at',
        ),
        migrations.RenameField(
            model_name='reservation',
            old_name='date_created',
            new_name='created_at',
        ),
        migrations.RenameField(
            model_name='salle',
            old_name='date_created',
            new_name='created_at',
        ),
        migrations.AddField(
            model_name='categorie',
            name='updated_at',
            field=models.DateTimeField(auto_now=True, null=True),
        ),
        migrations.AddField(
            model_name='client',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True, null=True),
        ),
        migrations.AddField(
            model_name='client',
            name='updated_at',
            field=models.DateTimeField(auto_now=True, null=True),
        ),
        migrations.AddField(
            model_name='materiel',
            name='updated_at',
            field=models.DateTimeField(auto_now=True, null=True),
        ),
        migrations.AddField(
            model_name='reservation',
            name='updated_at',
            field=models.DateTimeField(auto_now=True, null=True),
        ),
        migrations.AddField(
            model_name='salle',
            name='updated_at',
            field=models.DateTimeField(auto_now=True, null=True),
        ),
    ]
