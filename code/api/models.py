from django.db import models

# Create your models here.
class Materiel(models.Model):
    nom          = models.CharField(max_length=200, null=True)
    salle        = models.ForeignKey("Salle", null=True, on_delete=models.CASCADE)
    updated_at   = models.DateTimeField(auto_now=True,null=True)
    created_at  = models.DateTimeField(auto_now_add=True,null=True)
    
    
    def __str__(self):
        return self.nom
    
class Categorie(models.Model):
    type          = models.CharField(max_length=200, null=True)
    updated_at   = models.DateTimeField(auto_now=True,null=True)
    created_at  = models.DateTimeField(auto_now_add=True,null=True)
    
    def __str__(self):
        return self.type

class Salle(models.Model):
    #id = models.IntegerField(primary_key=True)
    numero       = models.IntegerField(null=True)
    description  = models.CharField(max_length=200, null=True)
    updated_at   = models.DateTimeField(auto_now=True,null=True)
    created_at  = models.DateTimeField(auto_now_add=True,null=True)
    
    categorie    = models.ForeignKey("Categorie", null=True, on_delete=models.CASCADE)
    #materiels    = models.ManyToManyField("Materiel")
    #clients      = models.ManyToManyField("Client", through='Reservation')
    
    def __str__(self):
        return str(self.numero)

class Client(models.Model):
    nom         = models.CharField(max_length=200, null=True)
    salles      = models.ManyToManyField("Salle", through='Reservation')
    updated_at  = models.DateTimeField(auto_now=True,null=True)
    created_at  = models.DateTimeField(auto_now_add=True,null=True)
    
    def __str__(self):
        return self.nom


class Reservation(models.Model):
    dateDebut = models.DateTimeField(null=True)
    duree     = models.IntegerField(null=True)
    #unite     = models.CharField(max_length=50,null=False)
    client    = models.ForeignKey("Client", null=True, on_delete=models.CASCADE)
    Salle     = models.ForeignKey("salle", null=True, on_delete=models.CASCADE)
    
    
    created_at  = models.DateTimeField(auto_now_add=True,null=True)
    updated_at   = models.DateTimeField(auto_now=True,null=True)

    def __str__(self):
        return f"Réservation de la salle #{self.Salle} par {self.client}"
