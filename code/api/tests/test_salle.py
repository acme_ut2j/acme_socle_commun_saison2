from django.test import TestCase
from rest_framework import status
from rest_framework.test import APIClient

from api.models import Salle

# to run tests : python manage.py test api/tests --verbosity=2
# https://www.django-rest-framework.org/api-guide/status-codes/
# https://www.django-rest-framework.org/api-guide/testing/#apiclient

class ApiTestCase(TestCase):
    fixtures = ['salles']

    salle_url = 'http://localhost:8000/api/salle/'
    salle_new = {'id': 4 ,'numero': 18, 'description':'petite salle', 'categorie_id': 1}
    #salle_new = {'numero': "18"}
    salle_edited = salle_new.copy()
    salle_edited['description'] += "edited"
    #print(salle_edited['description'])

    salle2_new = {'id': 5 ,'numero': 12, 'description':'grande salle', 'categorie_id': 1}
    salle2_edited = salle2_new.copy()
    salle2_edited['description'] += "edited"

    def setUp(self):
        self.anonymous = APIClient()
        self.connected = APIClient()
        self.connected.login(username='lauren', password='secret')

    #client = RequestsClient()
    #response = client.get('http://testserver/users/')
    #assert response.status_code == 200
    
    def test_anonymous_can_get_all_salle(self):
        response = self.anonymous.get(self.salle_url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json()[0]['id'], 1)
        #print(response.json()[0])
        #print(response.json()[1])
        self.assertEqual(response.json()[0]['description'], "Salle dédiée à l'utilisation des ordinateurs mis à disposition pour une pratique informatique")
        self.assertEqual(response.json()[1]['id'], 2)
        self.assertEqual(response.json()[1]['description'], "Salle dédiée au déroulement des travaux dirigés")
    
    def test_anonymous_can_get_one_salle(self):
        response = self.anonymous.get(self.salle_url + '1/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json()['id'], 1)
        self.assertEqual(response.json()['description'], "Salle dédiée à l'utilisation des ordinateurs mis à disposition pour une pratique informatique")

    def test_anonymous_can_add_salle(self):
        response = self.anonymous.post(self.salle_url, self.salle_new)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        response = self.anonymous.get(self.salle_url + str(response.json()['id']) + '/')  
        self.assertEqual(response.json()['numero'], self.salle_new['numero'])

    def test_anonymous_can_update_salle(self):
        response = self.anonymous.put(self.salle_url + '1/', self.salle_edited)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response = self.anonymous.get(self.salle_url + '1/')
        #print(response.json()['description'])
        self.assertEqual(response.json()['description'], self.salle_edited['description'])
 
    def test_anonymous_can_delete_salle(self):
        response = self.anonymous.delete(self.salle_url + '1/')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        response = self.anonymous.get(self.salle_url + '1/')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    
    def test_connected_can_get_all_salle(self):
        response = self.connected.get(self.salle_url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json()[0]['id'], 1)
        #print(response.json()[0])
        #print(response.json()[1])
        self.assertEqual(response.json()[0]['description'], "Salle dédiée à l'utilisation des ordinateurs mis à disposition pour une pratique informatique")
        self.assertEqual(response.json()[1]['id'], 2)
        self.assertEqual(response.json()[1]['description'], "Salle dédiée au déroulement des travaux dirigés")

    def test_connected_can_get_one_salle(self):
        response = self.connected.get(self.salle_url + '1/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json()['id'], 1)
        self.assertEqual(response.json()['description'], "Salle dédiée à l'utilisation des ordinateurs mis à disposition pour une pratique informatique")


    def test_connected_can_add_salle(self):
        response = self.connected.post(self.salle_url, self.salle2_new)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        response = self.connected.get(self.salle_url + str(response.json()['id']) + '/')
        self.assertEqual(response.json()['numero'], self.salle2_new['numero'])

    def test_connected_can_update_salle(self):
        response = self.connected.put(self.salle_url + '1/', self.salle2_edited)
        #print(response)
        #print(response.json()['description'])
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        #print(response.json()['numero'])
        #response = self.anonymous.get(self.salle_url + '1/')
        #print(response.json()['description'])
        self.assertEqual(response.json()['description'], self.salle2_edited['description'])

    def test_connected_can_delete_salle(self):
        response = self.connected.delete(self.salle_url + '1/')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        response = self.connected.get(self.salle_url + '1/')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)