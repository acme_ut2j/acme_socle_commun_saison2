from django.contrib.auth.models import Group
from django.test import TestCase
from rest_framework import status
from rest_framework.test import APIClient

from api.models import Materiel

# to run tests : python manage.py test api/tests --verbosity=2
# https://www.django-rest-framework.org/api-guide/status-codes/
# https://www.django-rest-framework.org/api-guide/testing/#apiclient


class ApiTestCase(TestCase):
    fixtures = ['materiels']

    materiel_url = 'http://localhost:8000/api/materiel/'
    materiel_new = {'id':10,'nom': 'PC'}
    materiel_edited = materiel_new.copy()
    materiel_edited['nom'] += " edited"

    def setUp(self):
        self.anonymous = APIClient()
        self.connected = APIClient()
        self.connected.login(username='lauren', password='secret')

    def test_anonymous_can_get_all_materiel(self):
        response = self.anonymous.get(self.materiel_url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        #self.assertEqual(response.json()['id'], 1)
        self.assertEqual(response.json()[0]['id'], 1)
        #self.assertEqual(response.json()[0]['id'], 2)
        #self.assertEqual(response.json()[0]['nom'], "Ordinateurs")
        #self.assertEqual(response.json()[0]['salle_id'],18)
        self.assertEqual(response.json()[1]['id'], 2)
        #self.assertEqual(response.json()[1]['nom'], "stylo")
        #self.assertEqual(response.json()[1]['salle_id'],18)

    def test_anonymous_can_get_one_materiel(self):
        response = self.anonymous.get(self.materiel_url + '1/')
        #print(response)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        
        self.assertEqual(response.json()['id'], 1)
        self.assertEqual(response.json()['nom'], "Ordinateurs")
        #self.assertEqual(response.json()['salle_id'], 16)

    def test_anonymous_can_add_materiel(self):
        response = self.anonymous.post(self.materiel_url, self.materiel_new)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        response = self.anonymous.get(self.materiel_url + str(response.json()['id']) + '/')
        #self.assertEqual(response.json()['nom'], self.materiel_new['nom'])
        self.assertEqual(response.json()['nom'], self.materiel_new['nom'])


    def test_anonymous_can_update_materiel(self):
        response = self.anonymous.put(self.materiel_url + '1/', self.materiel_edited)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response = self.anonymous.get(self.materiel_url + '1/')
        self.assertEqual(response.json()['nom'], self.materiel_edited['nom'])
        

    def test_anonymous_can_delete_materiel(self):
        response = self.anonymous.delete(self.materiel_url + '1/')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        response = self.anonymous.get(self.materiel_url + '1/')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)


    def test_connected_can_get_all_materiel(self):
        response = self.connected.get(self.materiel_url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json()[0]['id'], 1)
        #self.assertEqual(response.json()[0]['nom'], "Ordinateurs")
        self.assertEqual(response.json()[1]['id'], 2)
        #self.assertEqual(response.json()[1]['nom'], "Tableau")

    def test_connected_can_get_one_materiel(self):
        response = self.connected.get(self.materiel_url + '1/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json()['id'], 1)
        #self.assertEqual(response.json()['nom'], "Ordinateurs")

    def test_connected_can_add_materiel(self):
        response = self.connected.post(self.materiel_url, self.materiel_new)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        response = self.connected.get(self.materiel_url + str(response.json()['id']) + '/')
        self.assertEqual(response.json()['nom'], self.materiel_new['nom'])
        #self.assertEqual(response.json()['id'], self.materiel_new['id'])

    def test_connected_can_update_materiel(self):
        response = self.connected.put(self.materiel_url + '1/', self.materiel_edited)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response = self.connected.get(self.materiel_url + '1/')
        self.assertEqual(response.json()['nom'], self.materiel_edited['nom'])

    def test_connected_can_delete_materiel(self):
        response = self.connected.delete(self.materiel_url + '1/')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        response = self.connected.get(self.materiel_url + '1/')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)