from django.test import TestCase
from rest_framework import status
from rest_framework.test import APIClient

from api.models import Client

class ApiTestCase(TestCase):
    fixtures = ['reservations']

    reservation_url = 'http://localhost:8000/api/reservation/'
    reservation_new = {'id': 3, 'Salle_id': 1, 'client_id' : 1, 'duree' : 2, }#'unite' : 'heures'}
    reservation_edited = reservation_new.copy()
    reservation_edited['duree'] += 4
    #print(reservation_edited)
    #a += 1 

    reservation2_new = {'id': 4, 'Salle_id': 2, 'client_id' : 2, 'duree' : 2} #'unite' : 'heures'}
    reservation2_edited = reservation2_new.copy()
    reservation2_edited['duree'] += 6
    
    def setUp(self):
        self.anonymous = APIClient()
        self.connected = APIClient()
        self.connected.login(username='lauren', password='secret')
    
    def test_anonymous_can_get_all_reservation(self):
        response = self.anonymous.get(self.reservation_url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json()[0]['id'], 1)
        #self.assertEqual(response.json()[0]['Salle'], 1)
        self.assertEqual(response.json()[1]['id'], 2)
        #self.assertEqual(response.json()[1]['Salle'], 2)
    
    def test_anonymous_can_get_one_reservation(self):
        response = self.anonymous.get(self.reservation_url + '1/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json()['id'], 1)
        #self.assertEqual(response.json()['Salle'], 1)
    
    def test_anonymous_can_add_reservation(self):
        response = self.anonymous.post(self.reservation_url, self.reservation_new)
        #print(response)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        response = self.anonymous.get(self.reservation_url + str(response.json()['id']) + '/')
        self.assertEqual(response.json()['id'], self.reservation_new['id'])
    
    def test_anonymous_can_update_reservation(self):
        response = self.anonymous.put(self.reservation_url + '1/', self.reservation_edited)
        #print(response.json()['unite'])
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response = self.anonymous.get(self.reservation_url + '1/')
        #print(response.json()['unite'])
        self.assertEqual(response.json()['duree'], self.reservation_edited['duree'])
    
    def test_anonymous_can_delete_reservation(self):
        response = self.anonymous.delete(self.reservation_url + '1/')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        response = self.anonymous.get(self.reservation_url + '1/')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)


    def test_connected_can_get_all_reservation(self):
        response = self.connected.get(self.reservation_url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json()[0]['id'], 1)
        #self.assertEqual(response.json()[0]['Salle'], 1)
        self.assertEqual(response.json()[1]['id'], 2)
        #self.assertEqual(response.json()[1]['Salle'], 2)
    
    def test_connected_can_get_one_reservation(self):
        response = self.connected.get(self.reservation_url + '1/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json()['id'], 1)
        #self.assertEqual(response.json()['Salle'], 1)
    
    def test_connected_can_add_reservation(self):
        response = self.connected.post(self.reservation_url, self.reservation2_new)
        #print(response)
        #print(response.json()['id'])
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        response = self.connected.get(self.reservation_url + str(response.json()['id']) + '/')
        #print(response.json()['id'])
        self.assertEqual(response.json()['id'], self.reservation2_new['id'])
    
    def test_connected_can_update_reservation(self):
        response = self.connected.put(self.reservation_url + '1/', self.reservation2_edited)
        #print(response.json()['unite'])
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response = self.connected.get(self.reservation_url + '1/')
        #print(response.json()['unite'])
        self.assertEqual(response.json()['duree'], self.reservation2_edited['duree'])
    
    def test_connected_can_delete_reservation(self):
        response = self.connected.delete(self.reservation_url + '1/')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        response = self.connected.get(self.reservation_url + '1/')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)