from django.test import TestCase
from rest_framework import status
from rest_framework.test import APIClient

from api.models import Client

class ApiTestCase(TestCase):
    fixtures = ['clients']

    client_url = 'http://localhost:8000/api/client/'
    client_new = {'nom': 'tutu'}
    client_edited = client_new.copy()
    client_edited['nom'] += " edited"
    
    def setUp(self):
        self.anonymous = APIClient()
        self.connected = APIClient()
        self.connected.login(username='lauren', password='secret')
    
    def test_anonymous_can_get_all_client(self):
        response = self.anonymous.get(self.client_url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json()[0]['id'], 1)
        self.assertEqual(response.json()[0]['nom'], "Dricks")
        self.assertEqual(response.json()[1]['id'], 2)
        self.assertEqual(response.json()[1]['nom'], "Honoris")
    
    def test_anonymous_can_get_one_client(self):
        response = self.anonymous.get(self.client_url + '1/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json()['id'], 1)
        self.assertEqual(response.json()['nom'], "Dricks")
    
    def test_anonymous_can_add_client(self):
        response = self.anonymous.post(self.client_url, self.client_new)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        response = self.anonymous.get(self.client_url + str(response.json()['id']) + '/')
        self.assertEqual(response.json()['nom'], self.client_new['nom'])
    
    def test_anonymous_can_update_client(self):
        response = self.anonymous.put(self.client_url + '1/', self.client_edited)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response = self.anonymous.get(self.client_url + '1/')
        self.assertEqual(response.json()['nom'], self.client_edited['nom'])
    
    def test_anonymous_can_delete_client(self):
        response = self.anonymous.delete(self.client_url + '1/')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        response = self.anonymous.get(self.client_url + '1/')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    
    def test_connected_can_get_all_client(self):
        response = self.connected.get(self.client_url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json()[0]['id'], 1)
        self.assertEqual(response.json()[0]['nom'], "Dricks")
        self.assertEqual(response.json()[1]['id'], 2)
        self.assertEqual(response.json()[1]['nom'], "Honoris")
    
    def test_connected_can_get_one_client(self):
        response = self.connected.get(self.client_url + '1/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json()['id'], 1)
        self.assertEqual(response.json()['nom'], "Dricks")
    
    def test_connected_can_add_client(self):
        response = self.connected.post(self.client_url, self.client_new)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        response = self.connected.get(self.client_url + str(response.json()['id']) + '/')
        self.assertEqual(response.json()['nom'], self.client_new['nom'])
    
    def test_connected_can_update_client(self):
        response = self.connected.put(self.client_url + '1/', self.client_edited)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response = self.connected.get(self.client_url + '1/')
        self.assertEqual(response.json()['nom'], self.client_edited['nom'])
    
    def test_connected_can_delete_client(self):
        response = self.connected.delete(self.client_url + '1/')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        response = self.connected.get(self.client_url + '1/')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)