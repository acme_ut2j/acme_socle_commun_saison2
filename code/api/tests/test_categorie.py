from django.test import TestCase
from rest_framework import status
from rest_framework.test import APIClient

from api.models import Categorie

class ApiTestCase(TestCase):
    fixtures = ['categories']

    categorie_url = 'http://localhost:8000/api/categorie/'
    categorie_new = {'type': 'toto'}
    categorie_edited = categorie_new.copy()
    categorie_edited['type'] += "edited"
    
    
    def setUp(self):
        self.anonymous = APIClient()
        self.connected = APIClient()
        self.connected.login(username='lauren', password='secret')
    
    def test_anonymous_can_get_all_categorie(self):
        response = self.anonymous.get(self.categorie_url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json()[0]['id'], 1)
        self.assertEqual(response.json()[0]['type'], "Studio")
        self.assertEqual(response.json()[1]['id'], 2)
        self.assertEqual(response.json()[1]['type'], "Standard")
    
    def test_anonymous_can_get_one_categorie(self):
        response = self.anonymous.get(self.categorie_url + '1/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json()['id'], 1)
        self.assertEqual(response.json()['type'], "Studio")
    
    def test_anonymous_can_add_categorie(self):
        response = self.anonymous.post(self.categorie_url, self.categorie_new)
        #print(response)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        response = self.anonymous.get(self.categorie_url + str(response.json()['id']) + '/')
        self.assertEqual(response.json()['type'], self.categorie_new['type'])
        
    
    def test_anonymous_can_update_categorie(self):
        response = self.anonymous.put(self.categorie_url + '1/', self.categorie_edited)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response = self.anonymous.get(self.categorie_url + '1/')
        self.assertEqual(response.json()['type'], self.categorie_edited['type'])
        #print(response.json()['type'])
    
    def test_anonymous_can_delete_categorie(self):
        response = self.anonymous.delete(self.categorie_url + '1/')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        response = self.anonymous.get(self.categorie_url + '1/')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)



    def test_connected_can_get_all_categorie(self):
        response = self.connected.get(self.categorie_url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json()[0]['id'], 1)
        self.assertEqual(response.json()[0]['type'], "Studio")
        self.assertEqual(response.json()[1]['id'], 2)
        self.assertEqual(response.json()[1]['type'], "Standard")
    
    def test_connected_can_get_one_categorie(self):
        response = self.connected.get(self.categorie_url + '1/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json()['id'], 1)
        self.assertEqual(response.json()['type'], "Studio")
    
    def test_connected_can_add_categorie(self):
        response = self.connected.post(self.categorie_url, self.categorie_new)
        #print(response)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        response = self.connected.get(self.categorie_url + str(response.json()['id']) + '/')
        self.assertEqual(response.json()['type'], self.categorie_new['type'])
        
    
    def test_connected_can_update_categorie(self):
        response = self.connected.put(self.categorie_url + '1/', self.categorie_edited)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response = self.connected.get(self.categorie_url + '1/')
        self.assertEqual(response.json()['type'], self.categorie_edited['type'])
        #print(response.json()['type'])
    
    def test_connected_can_delete_categorie(self):
        response = self.connected.delete(self.categorie_url + '1/')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        response = self.connected.get(self.categorie_url + '1/')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)