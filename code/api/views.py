from django.shortcuts import render
from rest_framework import generics, status, viewsets
from .serializers import *
from .models import *
from rest_framework.views import APIView
from rest_framework.response import Response
from django.http import JsonResponse

# Create your views here.

# Views de la classe Materiel
class materielView(generics.ListAPIView):
    queryset = Materiel.objects.all()
    serializer_class = MaterielSerializer
    
class materielDetailView(APIView):
    serializer_class = MaterielSerializer
    lookup_url_kwarg = 'nom'

    def get(self, request, format=None):
        nom = request.GET.get(self.lookup_url_kwarg)
        if nom != None:
            materiel = Materiel.objects.filter(nom=nom)
            if materiel.exists():
                data = MaterielSerializer(materiel[0]).data
                return Response(data, status=status.HTTP_200_OK)
            return Response({'Materiel non trouvé': 'Nom invalide.'}, status=status.HTTP_404_NOT_FOUND)

        return Response({'Mauvaise requete': 'Données invalides...'}, status=status.HTTP_400_BAD_REQUEST)

class ajouterMaterielView(APIView):
    serializer_class = MaterielSerializer
    
    def post(self, request, format=None):
        serializer = self.serializer_class(data=request.data)
        
        if serializer.is_valid():
            nom = serializer.data.get('nom')
            
            #what i'm going to add
            queryMateriel = Materiel.objects.filter(nom=nom)
            salle = serializer.data.get('salle')
            querySalle = Salle.objects.filter(id=salle)
            #queryset = Salle.objects.filter(id=salle)
            queryset = Materiel.objects.filter(nom=nom)
            
            if queryset.exists():
                return Response({'Mauvaise requete': 'Données existe deja...'}, status=status.HTTP_400_BAD_REQUEST)
            else:
                materiel = Materiel(nom=nom,salle=querySalle.first())
                materiel.save()
            
                return Response(MaterielSerializer(materiel).data, status=status.HTTP_201_CREATED)

        return Response({'Mauvaise requete': 'Données invalides...'}, status=status.HTTP_400_BAD_REQUEST)

class supprimerMaterielView(APIView):
    lookup_url_kwarg = 'nom'
    
    def delete(self, request, format=None):
        nom = request.GET.get(self.lookup_url_kwarg)
        if nom != None:
            materiel_result = Materiel.objects.filter(nom=nom)
            if materiel_result.exists():
                materiel = materiel_result[0]
                materiel.delete()
                
                return Response({'Message': 'Success'}, status=status.HTTP_200_OK)
            
            return Response({'Materiel non trouvé': 'Nom invalide.'}, status=status.HTTP_404_NOT_FOUND)
        
        return Response({'Mauvaise requete': 'Données invalides...'}, status=status.HTTP_400_BAD_REQUEST)



# ************************************************************************************************
# Views de la classe Salle
# ************************************************************************************************

class salleView(generics.ListAPIView):
    queryset = Salle.objects.all()
    serializer_class = SalleSerializer

class salleDetailView(APIView):
    serializer_class = SalleSerializer
    lookup_url_kwarg = 'numero'

    def get(self, request, format=None):
        numero = request.GET.get(self.lookup_url_kwarg)
        if numero != None:
            salle = Salle.objects.filter(numero=numero)
            if salle.exists():
                data = SalleSerializer(salle[0]).data
                return Response(data, status=status.HTTP_200_OK)
            return Response({'Salle non trouvé': 'numero invalide.'}, status=status.HTTP_404_NOT_FOUND)

        return Response({'Mauvaise requete': 'Données invalides...'}, status=status.HTTP_400_BAD_REQUEST)
    
class ajouterSalleView(APIView):
    serializer_class = SalleSerializer
    
    def post(self, request, format=None):
        serializer = self.serializer_class(data=request.data)
        
        if serializer.is_valid():
            numero       = serializer.data.get('numero')
            queryNumero = Salle.objects.filter(numero=numero)
            categorie    = serializer.data.get('categorie')
            queryCategorie = Categorie.objects.filter(id=categorie)
            description  = serializer.data.get('description')
            queryDescription = Salle.objects.filter(description=description)
            
            materiels    = serializer.data.get('materiels')
            
            queryset = Salle.objects.filter(numero=numero)
            if queryset.exists(): 
                salle = queryset[0]
                
                salle.categorie   = categorie
                salle.description = description
                salle.materiels.set(materiels)
                
                salle.save(update_fields=['categorie', 'description'])

                #return Response({'Mauvaise requete': 'Données invalides...'}, status=status.HTTP_400_BAD_REQUEST)
                
                return Response(SalleSerializer(salle).data, status=status.HTTP_200_OK)
            
            else:
                
                salle = Salle.objects.create()
                #salle = Salle(numero=numero,description=description,categorie=queryCategorie.first())
                
                salle.numero = numero
                salle.categorie = categorie
                salle.description = description
                salle.materiels.set(materiels)
                #salle = Salle(numero=numero)
                
                salle.save()
                
                return Response(SalleSerializer(salle).data, status=status.HTTP_201_CREATED)
    
        return Response({'Mauvaise requete': 'Données invalides...'}, status=status.HTTP_400_BAD_REQUEST)

class supprimerSalleView(APIView):
    lookup_url_kwarg = 'numero'
    
    def delete(self, request, format=None):
        numero = request.GET.get(self.lookup_url_kwarg)
        if numero != None:
            salle_result = Salle.objects.filter(numero=numero)
            if salle_result.exists():
                salle = salle_result[0]
                salle.delete()
                
                return Response({'Message': 'Success'}, status=status.HTTP_200_OK)
            
            return Response({'Salle non trouvé': 'Numero invalide.'}, status=status.HTTP_404_NOT_FOUND)
        
        return Response({'Mauvaise requete': 'Données invalides...'}, status=status.HTTP_400_BAD_REQUEST)


# ************************************************************************************************
# Views de la classe Client
# ************************************************************************************************

class clientView(generics.ListAPIView):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer

class clientDetailView(APIView):
    serializer_class = ClientSerializer
    lookup_url_kwarg = 'nom'

    def get(self, request, format=None):
        nom = request.GET.get(self.lookup_url_kwarg)
        if nom != None:
            client = Client.objects.filter(nom=nom)
            if client.exists():
                data = ClientSerializer(client[0]).data
                return Response(data, status=status.HTTP_200_OK)
            return Response({'Client non trouvé': 'Nom invalide.'}, status=status.HTTP_404_NOT_FOUND)

        return Response({'Mauvaise requete': 'Données invalides...'}, status=status.HTTP_400_BAD_REQUEST)

class ajouterClientView(APIView):
    serializer_class = ClientSerializer
    
    def post(self, request, format=None):
        serializer = self.serializer_class(data=request.data)
        
        if serializer.is_valid():
            nom = serializer.data.get('nom')
            queryset = Client.objects.filter(nom=nom)
            if queryset.exists():
                return Response({'Mauvaise requete': 'Données existe deja...'}, status=status.HTTP_400_BAD_REQUEST)
            else:
                client = Client(nom=nom)
                client.save()
            
                return Response(ClientSerializer(client).data, status=status.HTTP_201_CREATED)

        return Response({'Mauvaise requete': 'Données invalides...'}, status=status.HTTP_400_BAD_REQUEST)

class supprimerClientView(APIView):
    lookup_url_kwarg = 'nom'
    
    def delete(self, request, format=None):
        nom = request.GET.get(self.lookup_url_kwarg)
        if nom != None:
            client_result = Client.objects.filter(nom=nom)
            if client_result.exists():
                client = client_result[0]
                client.delete()
                
                return Response({'Message': 'Success'}, status=status.HTTP_200_OK)
            
            return Response({'Client non trouvé': 'Nom invalide.'}, status=status.HTTP_404_NOT_FOUND)
        
        return Response({'Mauvaise requete': 'Données invalides...'}, status=status.HTTP_400_BAD_REQUEST)


# ************************************************************************************************
# Views de la classe Categorie
# ************************************************************************************************
class categorieView(generics.ListAPIView):
    queryset = Categorie.objects.all()
    serializer_class = CategorieSerializer

class categorieDetailView(APIView):
    serializer_class = CategorieSerializer
    lookup_url_kwarg = 'type'

    def get(self, request, format=None):
        type = request.GET.get(self.lookup_url_kwarg)
        if type != None:
            categorie = Categorie.objects.filter(type=type)
            if categorie.exists():
                data = CategorieSerializer(categorie[0]).data
                return Response(data, status=status.HTTP_200_OK)
            return Response({'Catégorie non trouvée': 'Type invalide.'}, status=status.HTTP_404_NOT_FOUND)

        return Response({'Mauvaise requete': 'Données invalides...'}, status=status.HTTP_400_BAD_REQUEST)

class ajouterCategorieView(APIView):
    serializer_class = CategorieSerializer
    
    def post(self, request, format=None):
        serializer = self.serializer_class(data=request.data)
        
        if serializer.is_valid():
            type = serializer.data.get('type')
            queryset = Categorie.objects.filter(type=type)
            if queryset.exists():
                return Response({'Mauvaise requete': 'Données existe deja...'}, status=status.HTTP_400_BAD_REQUEST)
            else:
                categorie = Categorie(type=type)
                categorie.save()
            
                return Response(CategorieSerializer(categorie).data, status=status.HTTP_201_CREATED)

        return Response({'Mauvaise requete': 'Données invalides...'}, status=status.HTTP_400_BAD_REQUEST)

class supprimerCategorieView(APIView):
    lookup_url_kwarg = 'type'
    
    def delete(self, request, format=None):
        type = request.GET.get(self.lookup_url_kwarg)
        if type != None:
            categorie_result = Categorie.objects.filter(type=type)
            if categorie_result.exists():
                categorie = categorie_result[0]
                categorie.delete()
                
                return Response({'Message': 'Success'}, status=status.HTTP_200_OK)
            
            return Response({'Catégorie non trouvée': 'Nom invalide.'}, status=status.HTTP_404_NOT_FOUND)
        
        return Response({'Mauvaise requete': 'Données invalides...'}, status=status.HTTP_400_BAD_REQUEST)




# ************************************************************************************************
# Views de la classe Reservation
# ************************************************************************************************


class reservationView(generics.ListAPIView):
    queryset = Reservation.objects.all()
    serializer_class = ReservationSerializer

class reservationDetailView(APIView):
    serializer_class = ReservationSerializer
    lookup_url_kwarg1 = 'creneau'
    lookup_url_kwarg2 = 'salle'

    def get(self, request, format=None):
        creneau = request.GET.get(self.lookup_url_kwarg1)
        salle   = request.GET.get(self.lookup_url_kwarg2)
        if creneau != None and salle != None:
            reservation = Reservation.objects.filter(creneau=creneau, Salle=salle)
            if reservation.exists():
                data = ReservationSerializer(reservation[0]).data
                return Response(data, status=status.HTTP_200_OK)
            return Response({'Reservation non trouvé': 'creneau invalide.'}, status=status.HTTP_404_NOT_FOUND)

        return Response({'Mauvaise requete': 'Données invalides...'}, status=status.HTTP_400_BAD_REQUEST)

class creerReservationView(APIView):
    serializer_class = ReservationSerializer
    
    def post(self, request, format=None):
        serializer = self.serializer_class(data=request.data)
        
        if serializer.is_valid():
            creneau       = serializer.data.get('creneau')
            #unite = serializer.data.get('unite')
            #queryUnite = Reservation.objects.filter(unite=unite)
            client = serializer.data.get('client')
            queryClient = Client.objects.filter(id=client)
            salle  = serializer.data.get('Salle')
            querySalle = Salle.objects.filter(id=salle)
            
            queryset       = Reservation.objects.filter(creneau=creneau)
            queryset_Salle = Reservation.objects.filter(Salle=salle)
            if queryset.exists() and queryset_Salle.exists():
                return Response({'Mauvaise requete': 'Salle deja reserve dans ce creneau..'}, status=status.HTTP_400_BAD_REQUEST)
            
            else:    
                reservation = Reservation(creneau=creneau,#unite=unite,
                                        client=queryClient.first(),
                                        Salle=querySalle.first())
                reservation.save()
                    
                return Response(ReservationSerializer(reservation).data, status=status.HTTP_201_CREATED)
    
        return Response({'Mauvaise requete': 'Données invalides...'}, status=status.HTTP_400_BAD_REQUEST)
    
class annulerReservationView(APIView):
    lookup_url_kwarg = 'salle'
    
    def delete(self, request, format=None):
        salle = request.GET.get(self.lookup_url_kwarg)
        if salle != None:
            reservation_result = Reservation.objects.filter(salle=salle)
            if reservation_result.exists():
                reservation = reservation_result[0]
                reservation.delete()
                
                return Response({'Message': 'Success'}, status=status.HTTP_200_OK)
            
            return Response({'Salle de reservation non trouvé': 'id invalide.'}, status=status.HTTP_404_NOT_FOUND)
        
        return Response({'Mauvaise requete': 'Données invalides...'}, status=status.HTTP_400_BAD_REQUEST)