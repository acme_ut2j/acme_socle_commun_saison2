from rest_framework import routers
from .viewsets import * 

router = routers.DefaultRouter()

router.register(r'materiel', MaterielViewSet)
router.register(r'categorie', CategorieViewSet)
router.register(r'salle', SalleViewSet)
router.register(r'client', ClientViewSet)
router.register(r'reservation', ReservationViewSet)

urlpatterns = router.urls
